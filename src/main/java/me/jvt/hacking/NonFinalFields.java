package me.jvt.hacking;

public class NonFinalFields {
  private int count;
  private String version;

  public NonFinalFields(int count, String version) {

    this.count = count;
    this.version = version;
  }

  public int getCount() {
    return count;
  }

  public String getVersion() {
    return version;
  }
}

package me.jvt.hacking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BadBean {
    @Autowired
    private Injected field;
}

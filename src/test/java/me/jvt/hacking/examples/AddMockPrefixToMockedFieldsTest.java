package me.jvt.hacking.examples;

import me.jvt.hacking.BadBean;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class AddMockPrefixToMockedFieldsTest {
  @Mock private BadBean bean;

  @Test
  void isNotNull() {
    // not a great test, but checks that Mockito is working
    assertThat(bean).isNotNull();
  }
}

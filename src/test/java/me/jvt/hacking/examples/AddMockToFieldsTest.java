package me.jvt.hacking.examples;

import me.jvt.hacking.BadBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class AddMockToFieldsTest {
    private List<BadBean> mockBeans;

    @BeforeEach
    void setup() {
        mockBeans = mock(List.class);
    }

    @Test
    void isNotNull() {
        // not a great test, but checks that Mockito is working
        assertThat(mockBeans).isNotNull();
    }
}

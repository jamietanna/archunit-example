package me.jvt.hacking.architecture;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.Nested;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import static com.tngtech.archunit.base.DescribedPredicate.describe;
import static com.tngtech.archunit.base.DescribedPredicate.equalTo;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;

@AnalyzeClasses(packages = "me.jvt.hacking")
class ArchitectureTest {

  @ArchTest
  ArchRule fieldsShouldNotBeAutowired =
      classes()
          .that()
          .haveNameNotMatching(".*Test")
          .and()
          .areNotAnnotatedWith(Nested.class)
          .and()
          .containAnyFieldsThat(
              describe(
                  "are Autowired by Spring",
                  f -> f.tryGetAnnotationOfType(Autowired.class).isPresent()))
          .should()
          .containNumberOfElements(equalTo(0));

  @ArchTest
  ArchRule alwaysFinalFields =
      classes()
          .that()
          .haveNameNotMatching(".*Test")
          .and()
          .areNotAnnotatedWith(Nested.class)
          .and()
          .haveNameNotMatching(".*Dao")
          .and()
          .resideOutsideOfPackage("..architecture")
          .should()
          .haveOnlyFinalFields();

  @ArchTest
  ArchRule mockAnnotationRequiresMockPrefix =
      fields()
          .that()
          .areDeclaredInClassesThat()
          .resideOutsideOfPackage("..architecture")
          .and()
          .areDeclaredInClassesThat()
          .haveNameMatching(".*Test")
          .or()
          .areDeclaredInClassesThat()
          .areAnnotatedWith(Nested.class)
          .and()
          .areAnnotatedWith(Mock.class)
          .should()
          .haveNameMatching("mock.*");

  @ArchTest
  ArchRule mockPrefixRequiresMockAnnotation =
      fields()
          .that()
          .areDeclaredInClassesThat()
          .resideOutsideOfPackage("..architecture")
          .and()
          .areDeclaredInClassesThat()
          .haveNameMatching(".*Test")
          .or()
          .areDeclaredInClassesThat()
          .areAnnotatedWith(Nested.class)
          .and()
          .haveNameMatching("mock.*")
          .should()
          .beAnnotatedWith(Mock.class);
}
